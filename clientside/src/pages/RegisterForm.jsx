import { Link } from "react-router-dom";
import '../styles/loginForm.css'
import { useForm } from "react-hook-form";
import { useState } from "react";
import { useNavigate } from "react-router-dom";
import registerApi from '../api/registerApi';

export function RegisterForm() {
    const navigate = useNavigate();
    const onSubmit = async (data) => {
        await registerApi(data);
        setTimeout(() => {
            navigate("/api/auth/login", { replace: true });
            }, 1000);
    };
    const { register, handleSubmit, formState: { errors }} = useForm();
    return (
        <section className="login-section">
        <div className="login-wrap">
            <h2>
            Sign <br></br>Up
            </h2>
            <form
            id="login-form"
            onSubmit={handleSubmit(onSubmit)}
            autoComplete="off"
            >
            <input type="text" placeholder="Email" {...register("email", {required: true})} />
            {errors?.email && { type: "required" } && (
                <span className="error-auth">*Email is required</span>
            )}
            <input type="password" placeholder="Password" {...register("password", {required: true})} />
            {errors?.password && { type: "required" } && (
                <span className="error-auth">*Password is required</span>
            )}
            <input type="tetx" placeholder="Role"  style={{textTransform: "uppercase"}} {...register("role", {required: true})} />
            {errors?.role && { type: "required" } && (
                <span className="error-auth">*Role is required</span>
            )}
            <button className="btn-submit" type="submit">Create new account</button>
            </form>
        </div>
        </section>
    );
}
