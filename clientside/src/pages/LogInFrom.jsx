import { Link } from "react-router-dom";
import '../styles/loginForm.css'
import { useForm } from "react-hook-form";
import { useState } from "react";
import { useNavigate } from "react-router-dom";
import loginApi from '../api/loginApi';
import forgotPassword from '../api/forgotPassword';

export function LogInForm() {
    const [ forgotPass, setForgotPass ] = useState(false);
    const navigate = useNavigate();
    const onSubmit = async (data) => {
        const response = await loginApi(data);
        console.log(response);
        setTimeout(() => {
        navigate("/api/main/new_loads", { replace: true });
        }, 1000);
    };
    const { register, getValues, handleSubmit, formState: { errors }} = useForm();
    return (
        <section className="login-section">
        <div className="login-wrap">
            <h2>
            Sign <br></br>in
            </h2>
            <form
            id="login-form"
            onSubmit={handleSubmit(onSubmit)}
            autoComplete="off"
            >
            <input type="text" placeholder="Email" {...register("email", {required: true})} />
            {errors?.email && { type: "required" } && (
                <span className="error-auth">*Email is required</span>
            )}
            <input type="password" placeholder="Password" {...register("password", {required: true})} />
            {errors?.password && { type: "required" } && (
                <span className="error-auth">*Password is required</span>
            )}
            <Link to="/api/auth/register" className="registration-link">
                Don`t have an account?
            </Link>
            <button className="btn-forgot" onClick={ (event) => {
                event.preventDefault();
                const email = getValues("email");
                setForgotPass(true);
                forgotPassword(email);
            }}>Forgot password</button>
            {forgotPass && (<span className="newpass">New password has been sent to your email</span>)}
            <button className="btn-submit" type="submit">log into the account</button>
            </form>
        </div>
        </section>
    );
}
