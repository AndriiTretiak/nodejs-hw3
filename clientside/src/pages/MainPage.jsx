import React from 'react'
import {Navigation} from '../components/Navigation';
import {Header} from '../components/Header';
import {Route, Routes, Outlet} from 'react-router-dom'
import '../styles/mainPage.css'
export function MainPage() {
  return (
    <>  
        <Header />
        <div className='main-page-wrap'>
            <Navigation />
            <Outlet />
        </div>
    </>
  )
}