import axios from "axios";

const loginApi = async (data) => {
  try {
    const response = await axios({
      method: "post",
      url: "http://localhost:8080/api/auth/login",
      data: {
        email: data.email,
        password: data.password,
      },
    });
    localStorage.setItem("token", JSON.stringify(response.data.jwt_token));
    return response;
  } catch (err) {
    console.log(err.response);
    return err.response;
  }
};

export default loginApi;
