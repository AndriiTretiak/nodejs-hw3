import axios from "axios";

export const getUserInfo = async () => {
  const token = localStorage.getItem("token");
  console.log(token);
  return await axios({
    method: "get",
    url: "http://localhost:8080/api/users/me",
    headers: { Authorization: `Bearer ${token}` },
  });
};
