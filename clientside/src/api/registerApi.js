import axios from "axios";

const registerApi = async (data) => {
  try {
    const response = await axios({
      method: "post",
      url: "http://localhost:8080/api/auth/register",
      data: {
        email: data.email,
        password: data.password,
        role: data.role.toUpperCase(),
      },
    });
    return response;
  } catch (err) {
    console.log(err.response);
    return err.response;
  }
};

export default registerApi;
