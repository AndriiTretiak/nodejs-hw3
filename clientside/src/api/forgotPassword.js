import axios from "axios";

const forgotPassword = async (data) => {
  try {
    const response = await axios({
      method: "post",
      url: "http://localhost:8080/api/auth/forgot_password",
      data: {
        email: data,
      },
    });
    console.log(response);
    return response;
  } catch (err) {
    console.log(err.response);
    return err.response;
  }
};

export default forgotPassword;
