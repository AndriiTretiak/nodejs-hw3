import "./App.css";
import { Route, Routes } from "react-router-dom";
import { LogInForm } from "./pages/LogInFrom";
import { RegisterForm } from "./pages/RegisterForm";
import { MainPage } from "./pages/MainPage";
import { Profile } from "./components/Profile";

function App() {
  return (
    <>
      <Routes>
        <Route path="/api/auth/login" element={<LogInForm />} />
        <Route path="/api/auth/register" element={<RegisterForm />} />
        <Route path="/api/main" element={<MainPage />}>
          <Route path="/api/main/new_loads" element={<Profile />} />
          <Route path="/api/main/posted_loads" element={<Profile />} />
          <Route path="/api/main/assigned_loads" element={<Profile />} />
          <Route path="/api/main/history" element={<Profile />} />
          <Route path="/api/main/profile" element={<Profile />} />
        </Route>
      </Routes>
    </>
  );
}

export default App;
