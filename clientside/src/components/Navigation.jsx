import React from 'react'
import { NavLink } from 'react-router-dom'
import '../styles/navigation.css'
export function Navigation() {
  return (
    <section>
        <span className='nav-elements'>
          <NavLink to="/api/main/new_loads" className={({ isActive }) => (isActive ? 'nav-element-active' : 'nav-element')}>NEW LOADS</NavLink>
          <NavLink to="/api/main/posted_loads"  className={({ isActive }) => (isActive ? 'nav-element-active' : 'nav-element')}>POSTED LOADS</NavLink>
          <NavLink to="/api/main/assigned_loads"  className={({ isActive }) => (isActive ? 'nav-element-active' : 'nav-element')}>ASSIGNED LOADS</NavLink>
          <NavLink to="/api/main/history"  className={({ isActive }) => (isActive ? 'nav-element-active' : 'nav-element')}>HISTORY</NavLink>
          <NavLink to="/api/main/profile"  className={({ isActive }) => (isActive ? 'nav-element-active' : 'nav-element')}>PROFILE</NavLink>
        </span>
    </section>
  )
}
