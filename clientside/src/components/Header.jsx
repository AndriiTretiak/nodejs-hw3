import React from 'react'
import '../styles/header.css'

export function Header() {
  return (
    <section className='header'>
        <div className='header-content'>
            <h1 className='header-title'>Delivery</h1>
        </div>
        <button>LOGOUT</button>
    </section>
  )
}
