const mongoose = require("mongoose");

const User = mongoose.model("User", {
  role: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
  created_date: {
    type: String,
    required: true,
  },
});

module.exports = {
  User,
};
