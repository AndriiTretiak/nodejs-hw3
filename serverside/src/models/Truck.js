const mongoose = require("mongoose");

const Truck = mongoose.model("Truck", {
  created_by: {
    type: String,
    required: true,
    unique: false,
  },
  assigned_to: {
    type: String,
  },
  type: {
    type: String,
  },
  status: {
    type: String,
    default: "IS",
  },
  dimensions: {
    width: {
      type: Number,
      required: true,
    },
    height: {
      type: Number,
      required: true,
    },
    length: {
      type: Number,
      required: true,
    },
  },
  payload: {
    type: Number,
    required: true,
  },
  created_date: {
    type: String,
    required: true,
  },
});

module.exports = {
  Truck,
};
