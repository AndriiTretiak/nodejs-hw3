const mongoose = require("mongoose");
const logSchema = mongoose.Schema({
  message: {
    type: String,
    required: true,
  },
  time: {
    type: Date,
    required: true,
  },
});

const Load = mongoose.model("Load", {
  created_by: {
    type: String,
    required: true,
  },
  assigned_to: {
    type: String,
    required: false,
  },
  status: {
    type: String,
    required: false,
    default: "NEW",
  },
  state: {
    type: String,
    required: false,
  },
  name: {
    type: String,
    required: true,
  },
  payload: {
    type: Number,
    required: true,
  },
  pickup_address: {
    type: String,
    required: true,
  },
  delivery_address: {
    type: String,
    required: true,
  },
  dimensions: {
    type: Object,
    required: true,
    properties: {
      width: { default: null, type: Number },
      length: { default: null, type: Number },
      height: { default: null, type: Number },
    },
  },
  logs: {
    type: [logSchema],
    required: false,
  },
  created_date: {
    type: Date,
    required: true,
  },
});

module.exports = {
  Load,
};
