const TruckService = require("../services/truckService");

class TruckController {
  async showTrucks(req, res, next) {
    try {
      const trucksData = await TruckService.showTrucks();
      return await res.status(200).json({ trucks: trucksData });
    } catch (err) {
      return res.status(400).send({
        message: err.message,
      });
    }
  }
  async addTruck(req, res, next) {
    try {
      const userId = req.user.userId;
      const { type, dimensions, payload } = req.body;
      const userData = await TruckService.addTruck(
        type,
        userId,
        dimensions,
        payload
      );
      return await res.json(userData);
    } catch (err) {
      return res.status(400).send({
        message: err.message,
      });
    }
  }
  async showTruckById(req, res, next) {
    try {
      const truckId = req.params.id;
      const trucksData = await TruckService.showTruckById(truckId);
      return await res.status(200).json({ truck: trucksData });
    } catch (err) {
      return res.status(400).send({
        message: err.message,
      });
    }
  }
  async updateTruck(req, res, next) {
    try {
      const newType = req.body.type;
      const truckId = req.params.id;
      await TruckService.updateTruck(truckId, newType);
      return res.status(200).json({ message: "Truck updated successfully" });
    } catch (err) {
      return res.status(400).send({
        message: err.message,
      });
    }
  }
  async deleteTruck(req, res, next) {
    try {
      const truckId = req.params.id;
      await TruckService.deleteTruck(truckId);
      return res.status(200).json({ message: "Truck deleted successfully" });
    } catch (err) {
      return res.status(400).send({
        message: err.message,
      });
    }
  }
  async assignTruck(req, res, next) {
    try {
      const userId = req.user.userId;
      const truckId = req.params.id;
      await TruckService.assignTruck(truckId, userId);
      return res.status(200).json({ message: "Truck assigned successfully" });
    } catch (err) {
      return res.status(400).send({
        message: err.message,
      });
    }
  }
}

module.exports = new TruckController();
