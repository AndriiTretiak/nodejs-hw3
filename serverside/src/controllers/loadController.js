const LoadService = require("../services/loadService");
const TruckService = require("../services/TruckService");

class LoadController {
  async showLoads(req, res, next) {
    try {
      const id = req.user.id;
      const loadsData = await LoadService.showLoads(id);
      return await res.status(200).json({ loads: loadsData });
    } catch (err) {
      return res.status(400).send({
        message: err.message,
      });
    }
  }
  async addLoad(req, res, next) {
    try {
      await LoadService.addLoad(req);
      return await res.json({ message: "Load created successfully" });
    } catch (err) {
      return res.status(400).send({
        message: err.message,
      });
    }
  }
  // async showActiveLoads(req, res, next) {
  //   try {
  //     const truckId = req.params.id;
  //     const trucksData = await LoadService.showLoadById(truckId);
  //     return await res.status(200).json({ truck: trucksData });
  //   } catch (err) {
  //     return res.status(400).send({
  //       message: err.message,
  //     });
  //   }
  // }
  // async iterateNextLoad(req, res, next) {
  //   try {
  //     const truckId = req.params.id;
  //     const trucksData = await TruckService.showTruckById(truckId);
  //     return await res.status(200).json({ truck: trucksData });
  //   } catch (err) {
  //     return res.status(400).send({
  //       message: err.message,
  //     });
  //   }
  // }
  async showLoadById(req, res, next) {
    try {
      const loadId = req.params.id;
      const loadData = await LoadService.showLoadById(loadId);
      return await res.status(200).json({ load: loadData });
    } catch (err) {
      return res.status(400).send({
        message: err.message,
      });
    }
  }
  async updateLoad(req, res, next) {
    try {
      const loadId = req.params.id;
      const data = req.body;
      await LoadService.updateTruck(loadId, data);
      return res.status(200).json({ message: "Load updated successfully" });
    } catch (err) {
      return res.status(400).send({
        message: err.message,
      });
    }
  }
  async deleteLoad(req, res, next) {
    try {
      const loadId = req.params.id;
      await LoadService.deleteLoad(loadId);
      return res.status(200).json({ message: "Load deleted successfully" });
    } catch (err) {
      return res.status(400).send({
        message: err.message,
      });
    }
  }
  async postLoadById(req, res, next) {
    const loadId = req.params.id;
    const load = await LoadService.updateUserLoadStatus(loadId, "POSTED");
    if (!load) {
      throw Error("Load not found");
    }
    const assignedTrucks = await TruckService.getAssignedTrucks();
    if (!assignedTrucks.length) {
      await LoadService.updateUserLoadStatus(loadId, "NEW");
      throw Error("No available trucks found");
    }
    const truckId = assignedTrucks[0]._id;
    const driverId = assignedTrucks[0].assigned_to;
    await LoadService.assignTruckToLoad(loadId, truckId, driverId);

    await LoadService.updateUserLoadStatus(loadId, "ASSIGNED");
    const loadParams = await LoadService.getLoadParams(loadId);
    const truckParams = await TruckService.getTruckParams(truckId);

    LoadService.compareParams(truckParams, loadParams);

    return res.status(200).json({
      message: "Load posted successfully",
      driver_found: true,
    });
  }
  // async getLoadInfoById(req, res, next) {
  //   try {
  //   } catch (err) {
  //     return res.status(400).send({
  //       message: err.message,
  //     });
  //   }
  // }
}

module.exports = new LoadController();
