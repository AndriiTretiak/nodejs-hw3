const { Load } = require("../models/Load.js");
const { Truck } = require("../models/Truck.js");
const moment = require("moment");

class LoadService {
  async showLoads(id) {
    try {
      return await Load.find({ created_by: id }, "-__v");
    } catch (err) {
      return res.status(400).send({
        message: err.message,
      });
    }
  }
  async addLoad(req) {
    console.log(req);
    try {
      const load = new Load({
        created_by: req.user.userId,
        name: req.body.name,
        payload: req.body.payload,
        pickup_address: req.body.pickup_address,
        delivery_address: req.body.delivery_address,
        dimensions: {
          width: req.body.dimensions.width,
          length: req.body.dimensions.length,
          height: req.body.dimensions.height,
        },
        created_date: moment().format(),
      });
      return await load.save();
    } catch (err) {
      return res.status(400).send({
        message: err.message,
      });
    }
  }
  //   async showActiveLoads(req, res, next) {
  //     try {
  //       const truckId = req.params.id;
  //       const trucksData = await TruckService.showTruckById(truckId);
  //       return await res.status(200).json({ truck: trucksData });
  //     } catch (err) {
  //       return res.status(400).send({
  //         message: err.message,
  //       });
  //     }
  //   }
  //   async iterateNextLoad(req, res, next) {
  //     try {
  //       const truckId = req.params.id;
  //       const trucksData = await TruckService.showTruckById(truckId);
  //       return await res.status(200).json({ truck: trucksData });
  //     } catch (err) {
  //       return res.status(400).send({
  //         message: err.message,
  //       });
  //     }
  //   }
  async showLoadById(loadId) {
    try {
      return await Load.findById(loadId, "-__v");
    } catch (err) {
      return res.status(400).send({
        message: err.message,
      });
    }
  }
  async updateLoad(loadId, data) {
    try {
      return await Load.findByIdAndUpdate(loadId, data);
    } catch (err) {
      return res.status(400).send({
        message: err.message,
      });
    }
  }
  async deleteLoad(loadId) {
    try {
      return await Load.findByIdAndDelete(loadId);
    } catch (err) {
      return res.status(400).send({
        message: err.message,
      });
    }
  }
  async postLoadById(req, res, next) {
    try {
    } catch (err) {
      return res.status(400).send({
        message: err.message,
      });
    }
  }
  //   async getLoadInfoById(req, res, next) {
  //     try {
  //     } catch (err) {
  //       return res.status(400).send({
  //         message: err.message,
  //       });
  //     }
  //   }

  async updateUserLoadStatus(loadId, status) {
    return await Load.findByIdAndUpdate(loadId, { status: status });
  }

  async assignTruckToLoad(loadId, truckId, driverId) {
    await Truck.findByIdAndUpdate(truckId, { status: "OL" });

    return await Load.findByIdAndUpdate(loadId, {
      assigned_to: driverId,
      $set: { state: "En route to Pick Up" },
      $push: {
        logs: {
          message: `Load assigned to driver with id ${driverId}`,
          time: moment().format(),
        },
      },
    });
  }

  async getLoadParams(loadId) {
    const { payload, dimensions } = await Load.findById(loadId);
    return { payload, dimensions };
  }

  async compareParams(truckParams, loadParams) {
    if (loadParams.payload > truckParams.payload) {
      throw Error("The load dimensions are larger than the truck capacity");
    }

    if (loadParams.dimensions.width > truckParams.dimensions.width) {
      throw Error("The load dimensions are larger than the truck capacity");
    }

    if (loadParams.dimensions.height > truckParams.dimensions.height) {
      throw Error("The load dimensions are larger than the truck capacity");
    }

    if (loadParams.dimensions.length > truckParams.dimensions.length) {
      throw Error("The load dimensions are larger than the truck capacity");
    }
  }
}

module.exports = new LoadService();

//   const updateUserLoadStatus = async (loadID, status) => {
//     return await Load.findByIdAndUpdate(loadID, { status: status })
//   }

//   const assignTruckToLoad = async (loadID, truckID, driverID) => {
//     await Truck.findByIdAndUpdate(truckID, { status: 'OL' })

//     return await Load.findByIdAndUpdate(loadID, {
//       assigned_to: truckID,
//       $set: { state: 'En route to Pick Up' },
//       $push: {
//         logs: {
//           message: `Load assigned to driver with id ${driverID}`,
//           time: new Date()
//         }
//       }
//     })
//   }

//   const getLoadParams = async (loadID) => {
//     const { payload, dimensions } = await Load.findById(loadID)

//     return { payload, dimensions }
//   }

//   const getLoadsShippingInfo = async (userID) => {
//     return await Load.aggregate([
//       {
//         $match: {
//           created_by: new mongoose.mongo.ObjectId(userID),
//           status: 'ASSIGNED'
//         }
//       },
//       {
//         $lookup: {
//           from: 'trucks',
//           localField: 'assigned_to',
//           foreignField: '_id',
//           as: 'truck'
//         }
//       },
//       {
//         $unwind: {
//           path: '$truck'
//         }
//       }
//     ])
//   }
