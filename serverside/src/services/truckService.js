const { Truck } = require("../models/Truck.js");
const moment = require("moment");

class TruckService {
  async showTrucks() {
    try {
      return await Truck.find({}, "-__v");
    } catch (err) {
      return res.status(400).send({
        message: err.message,
      });
    }
  }
  async addTruck(type, userId, dimensions, payload) {
    try {
      const truck = new Truck({
        created_by: userId,
        assigned_to: null,
        type: type,
        status: "IS",
        dimensions: {
          width: dimensions.width,
          height: dimensions.height,
          length: dimensions.length,
        },
        payload: payload,
        created_date: moment().format(),
      });
      return await truck.save();
    } catch (err) {
      return res.status(400).send({
        message: err.message,
      });
    }
  }
  async showTruckById(truckId) {
    try {
      return await Truck.findOne({ _id: truckId }, "-__v");
    } catch (err) {
      return res.status(400).send({
        message: err.message,
      });
    }
  }
  async updateTruck(truckId, newType) {
    try {
      return Truck.findByIdAndUpdate(truckId, {
        $set: { type: newType },
      });
    } catch (err) {
      return res.status(400).send({
        message: err.message,
      });
    }
  }
  async deleteTruck(truckId) {
    try {
      return await Truck.findByIdAndDelete(truckId);
    } catch (err) {
      return res.status(400).send({
        message: err.message,
      });
    }
  }
  async assignTruck(truckId, userId) {
    try {
      return Truck.findByIdAndUpdate(truckId, {
        $set: { assigned_to: userId },
      });
    } catch (err) {
      return res.status(400).send({
        message: err.message,
      });
    }
  }

  async getAssignedTrucks() {
    return await Truck.aggregate([
      {
        $match: {
          status: "IS",
          assigned_to: {
            $ne: null,
          },
        },
      },
    ]);
  }
  async getTruckParams(truckId) {
    const { payload, dimensions } = await Truck.findById(truckId);
    return { payload, dimensions };
  }
}

module.exports = new TruckService();
